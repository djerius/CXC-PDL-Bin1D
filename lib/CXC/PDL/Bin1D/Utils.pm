package CXC::PDL::Bin1D::Utils;

use v5.10;
use strict;
use warnings;

use Exporter 'import';

our $VERSION = '0.28';

our @EXPORT_OK = qw[ _bitflags _flags ];

sub _bitflags {
    my $bit = 1;
    ## no critic (BuiltinFunctions::ProhibitComplexMappings)
    return shift(), 1, map { $bit <<= 1; $_ => $bit; } @_;
}

sub _flags {
    my $bits = shift;
    croak( "unknown flag: $_ " ) for grep { !defined $bits->{$_} } @_;
    my $mask;
    $mask |= $_ for @{$bits}{@_};
    return $mask;
}

1;

__END__

=head1 CXC::PDL::Bin1D::Utils - internal routines for CXC::PDL::Bin1D

=cut

